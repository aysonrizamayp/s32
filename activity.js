

let http = require("http")

let port = 4000


http.createServer((request, response) => {

// GET Homepage
	if(request.url == "" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to Booking System')
	}

// GET Profile
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your profile!')
	}

// GET Courses
	 if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Here’s our courses available')
	}

// POST Courses
	if(request.url == "/addCourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Add course to our resources')
	}


// PUT Courses
	if(request.url == "/updateCourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Update a course to our resources')
	}

//DELETE Courses
	if(request.url == "/archiveCourse" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Archive courses to our resources')
	}

}) .listen(port) 

console.log(`Server is running at localhost: ${port}`)



